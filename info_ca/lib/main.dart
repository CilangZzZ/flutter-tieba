import 'package:flutter/material.dart';
import 'package:info_ca/pages/articleDetailPage.dart';
import 'package:info_ca/pages/basePage.dart';
import 'package:info_ca/pages/createArticlePage.dart';
import 'package:info_ca/pages/loginPage.dart';
import 'package:info_ca/pages/registerPage.dart';



void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false, // 不显示右上角的 debug
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        // 注册路由表
        routes: {
          "/": (context) => const LoginPage(title: "登录"), // 登录路由
          "/register":(context) => const RegisterPage(title: "注册",),
          "/home": (context) => BasePage(),
          "/createArticle": (context) => CreateArticle(),
          "/articleDetail": (context) => ArticleDetailPage(),
        });
  }
}
