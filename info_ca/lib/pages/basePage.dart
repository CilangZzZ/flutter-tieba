import 'package:flutter/material.dart';
import 'package:info_ca/pages/belongPage.dart';
import 'package:info_ca/pages/homePage.dart';
import 'package:info_ca/pages/message.dart';
import 'package:info_ca/pages/personalPage.dart';

import '../pages/createArticlePage.dart';

class BasePage extends StatefulWidget {
  const BasePage({super.key});

  @override
  State<BasePage> createState() => _BasePageState();
}

class _BasePageState extends State<BasePage> {
  int _currentIndex = 0;
  final List<Widget> _page = [
    HomePage(),
    PersonalPage(),
    MessagePage(),
    BelongPage()
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _page[_currentIndex],
      bottomNavigationBar: buildBottomBar(context),
    );
  }

  // 底部导航栏
  Widget buildBottomBar(BuildContext context) {
    return Container(
      color: Color.fromARGB(0, 0, 0, 0),
      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
      height: MediaQuery.of(context).size.height * 0.1,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          IconButton(
            onPressed: () {
              onTabTapped(0);
            },
            icon: Icon(Icons.home),
          ),
          IconButton(
              onPressed: () {
                onTabTapped(3);
              },
              icon: Icon(Icons.follow_the_signs)),
          IconButton(
            onPressed: () {
              Navigator.pushNamed(context, "/createArticle");
            },
            icon: Icon(Icons.add),
          ),
          IconButton(
              onPressed: () {
                onTabTapped(2);
              },
              icon: Icon(Icons.message)),
          IconButton(
            onPressed: () {
              onTabTapped(1);
            },
            icon: Icon(Icons.person),
          )
        ],
      ),
    );
  }

  
}
