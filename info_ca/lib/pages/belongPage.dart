import 'dart:math';

import 'package:flutter/material.dart';

class BelongPage extends StatefulWidget {
  const BelongPage({super.key});

  @override
  State<BelongPage> createState() => _BelongPageState();
}

class _BelongPageState extends State<BelongPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
      children: [buildAppBar(context), buildBelongedList(),buildBelongWarpList()],
    ));
  }

  Widget buildAppBar(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 3,
          blurRadius: 7,
          offset: Offset(0, 3), // changes position of shadow
        ),
      ], color: Colors.white, borderRadius: BorderRadius.circular((10))),
      padding: EdgeInsets.all(5),
      child: Container(
        padding: EdgeInsets.all(10),
        child: Container(
          margin: EdgeInsets.all(5),
          child: Row(
            children: [],
          ),
        ),
      ),
    );
  }

  Widget buildBelongedList() {
    return Container(
      margin: EdgeInsets.all(10),
      // height: MediaQuery.of(context).size.height * 0.3,
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 3,
          blurRadius: 7,
          offset: Offset(0, 3), // changes position of shadow
        ),
      ], color: Colors.white, borderRadius: BorderRadius.circular((10))),
      padding: EdgeInsets.all(5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("关注的吧"),
          Container(
              height: MediaQuery.of(context).size.height * 0.2,
              child: ListView.builder(
                scrollDirection: Axis.horizontal, // 设置滚动方向为横向
                itemCount: 10,
                itemBuilder: (BuildContext context, int index) {
                  return buildBelongHoriontalItem();
                },
              )),
        ],
      ),
    );
  }

  Widget buildBelongHoriontalItem() {
    return Container(
      // height: MediaQuery.of(context).size.height * 0.6,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.all(10),
            height: 75,
            width: 75,
            child: Card(
              color: Colors.black54,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadiusDirectional.circular(10)),
              clipBehavior: Clip.antiAlias,
              child: Image(
                image: NetworkImage(
                    "https://th.bing.com/th/id/OIP.e7067ZH6BXgYS84TLYzywgHaHa?pid=ImgDet&rs=1"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Text("孙笑川吧"),
          Text("关注 2064")
        ],
      ),
    );
  }

  Widget buildBelongWarpList() {
    return Container(
      child: Wrap(),
    );
  }
}
