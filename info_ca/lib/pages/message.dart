import 'package:flutter/material.dart';

class MessagePage extends StatefulWidget {
  const MessagePage({super.key});

  @override
  State<MessagePage> createState() => _MessagePageState();
}

class _MessagePageState extends State<MessagePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: [
          buildTopBar(),
          buildMessageItem(),
          buildMessageItem(),
          buildMessageItem(),
          buildMessageItem(),
        ],
      ),
    );
  }

  Widget buildMessageList() {
    return Container(
      child: Column(
        children: [
          buildMessageItem(),
          buildMessageItem(),
          buildMessageItem(),
          buildMessageItem(),
          buildMessageItem(),
          buildMessageItem(),
          buildMessageItem(),
          buildMessageItem(),
        ],
      ),
    );
  }

  Widget buildMessageItem() {
    return Container(
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 3,
          blurRadius: 7,
          offset: Offset(0, 3), // changes position of shadow
        ),
      ], color: Colors.white, borderRadius: BorderRadius.circular((5))),
      // height: 100,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadiusDirectional.circular(10)),
            clipBehavior: Clip.antiAlias,
            child: Image.network(
              'https://images.pexels.com/photos/850359/pexels-photo-850359.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=375',
              width: 40,
              height: 40,
              fit: BoxFit.cover,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("折叠的消息"),
              Text("卖片哥发来了一条广告"),
            ],
          )
          // ListTile(
          //   title: Text("折叠的消息"),
          //   subtitle: Text("卖片哥发来了一条广告"),
          // )
        ],
      ),
    );
  }

  Widget buildTopBar() {
    return Container(
        margin: const EdgeInsets.all(10),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 3,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ], color: Colors.white, borderRadius: BorderRadius.circular((5))),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildVerticalButton(Icons.ac_unit_outlined, "@我的"),
                buildVerticalButton(Icons.one_k, "点赞"),
                buildVerticalButton(Icons.message_rounded, "回复"),
                buildVerticalButton(Icons.person_add_alt_1, "粉丝")
              ],
            )
          ],
        ));
  }

  Widget buildVerticalButton(IconData icon, String label) {
    return Container(
      margin: const EdgeInsets.all(5),
      child: TextButton(
        onPressed: () {
          // 在这里添加点击事件处理逻辑
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(
              icon,
              color: Colors.grey[800],
            ),
            Text(
              label,
              style: TextStyle(fontSize: 16, color: Colors.black54),
            ),
          ],
        ),
      ),
    );
  }
}
