import 'package:flutter/material.dart';

class PersonalPage extends StatefulWidget {
  const PersonalPage({super.key});

  @override
  State<PersonalPage> createState() => _PersonalPageState();
}

class _PersonalPageState extends State<PersonalPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      child: ListView(
        children: [
          Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            buildPersonalInfo(),
            buildInfoBar(),
            buildFunctionItem(),
            buildFunctionItem()
          ])
        ],
      ),
    );
  }

  // 个人主页
  Widget buildPersonalInfo() {
    return Container(
      margin: const EdgeInsets.all(10),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 3,
          blurRadius: 7,
          offset: Offset(0, 3), // changes position of shadow
        ),
      ], color: Colors.white, borderRadius: BorderRadius.circular((5))),
      child: ListTile(
        contentPadding: EdgeInsets.all(5),
        leading: Card(
          color: Colors.black54,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadiusDirectional.circular(10)),
          clipBehavior: Clip.antiAlias,
          child: Image(
            image: NetworkImage(
                "http://q2.qlogo.cn/headimg_dl?dst_uin=1019383856&spec=100"),
            fit: BoxFit.cover,
          ),
        ),
        title: Row(
          children: [
            Text("UserName"),
          ],
        ),
        subtitle: Text("关注8 粉丝4"),
      ),
    );
  }

  // 信息导航栏
  Widget buildInfoBar() {
    return Container(
        margin: const EdgeInsets.all(10),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 3,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ], color: Colors.white, borderRadius: BorderRadius.circular((5))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            buildVerticalButton(4.toString(), "帖子"),
            buildVerticalButton(91.toString(), "关注"),
            buildVerticalButton(24.toString(), "收藏"),
            buildVerticalButton(300.toString(), "历史")
          ],
        ));
  }

  Widget buildFunctionItem() {
    return Container(
        margin: const EdgeInsets.all(10),
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 3,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ], color: Colors.white, borderRadius: BorderRadius.circular((5))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            buildHorizontalButton(Icons.person, "服务中心"),
            buildHorizontalButton(Icons.person, "个人中心"),
            buildHorizontalButton(Icons.settings, "软件设置"),
            buildHorizontalButton(Icons.person_pin, "联系客服"),
            buildHorizontalButton(Icons.add, "申请贴吧"),
            buildHorizontalButton(Icons.exit_to_app, "退出登录"),
            buildHorizontalButton(Icons.more_horiz, "更多")
          ],
        ));
  }

  Widget buildHorizontalButton(IconData icon, String label) {
    return Container(
      margin: const EdgeInsets.all(5),
      child: TextButton(
        onPressed: () {
          // 在这里添加点击事件处理逻辑
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(
              icon,
              color: Colors.grey[800],
            ),
            Text(
              label,
              style: TextStyle(fontSize: 16, color: Colors.black54),
            ),
          ],
        ),
      ),
    );
  }
}

Widget buildVerticalButton(String data, String label) {
  return TextButton(
    onPressed: () {
      // 在这里添加点击事件处理逻辑
    },
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          data,
          style: TextStyle(fontSize: 20, color: Colors.black),
        ),
        Text(
          label,
          style: TextStyle(fontSize: 16, color: Colors.black54),
        ),
      ],
    ),
  );
}
