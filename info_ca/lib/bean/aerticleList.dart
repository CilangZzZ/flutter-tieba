// ignore_for_file: file_names
import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;



class ArticleList <T>{
  final int code;
  final String msg;
  final List<T> result;
  final DateTime time;

  const ArticleList({
  required this.code,
  required this.msg,
  required this.result,
  required this.time,
});

  factory ArticleList.fromJson(Map<String, dynamic> json) {
    return ArticleList(
      code: json['code'] as int,
      msg: json['msg'] as String,
      result: json['result'] as List<T>,
      time: json['time'] as DateTime,
    );
  }
}
